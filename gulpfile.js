var path = require('path');
var del = require("del");
var gulp = require('gulp');
var $ = require('gulp-load-plugins')();
var runSequence = require('run-sequence');
var autoprefixer = require('autoprefixer');
var browserSync = require('browser-sync').create();

/** VARIABLES **/
var FONT_NAME = 'iContoAwesomeNeue';
var FONT_DISPLAY_NAME = 'Awesome Neue';
var PATHS = {
	dist: "./font",
	src: {
		styles: './src/scss/**/*.scss',
		svg: './src/svg/*.svg',
		templates: {
			all: './src/templates/*',
			css: './src/templates/css.ejs',
			html: './src/templates/html.ejs'
		}
	},
	manifest: {
		name: 'manifest.json'
	}
};

gulp.task('clean', function () {
	del(PATHS.dist)
});

gulp.task('iconfont', function () {
	return gulp.src(PATHS.src.svg)
		.pipe($.iconfont({
			fontName: FONT_NAME,
			formats: ['ttf', 'eot', 'woff', 'svg'],
			fontHeight: 1000,
			//normalize: true,
			//ascent: 500,
			//descent: 300,
		}))
		.on('glyphs', function (glyphs, options) {

			// GENERATE CSS TEMPLATE

			gulp.src(PATHS.src.templates.css)
				.pipe($.consolidate('lodash', {
					glyphs: glyphs,
					fontName: FONT_NAME,
					fontPath: 'fonts/',
					className: 'ic'
				}))
				.pipe($.rename({
					basename: FONT_NAME.toLocaleLowerCase(),
					extname: '.css'
				}))
				.pipe(gulp.dest(PATHS.dist));

			// GENERATE HTML TEMPLATE

			gulp.src(PATHS.src.templates.html)
				.pipe($.consolidate('lodash', {
					glyphs: glyphs,
					fontName: FONT_NAME,
					fontDisplayName: FONT_DISPLAY_NAME,
					headerIcon: 'alol',
					fontPath: 'fonts/',
					className: 'ic'
				}))
				.pipe($.rename('index.html'))
				.pipe(gulp.dest(PATHS.dist));
		})
		.pipe($.rev())
		.pipe(gulp.dest('./font/fonts'))
		.pipe($.rev.manifest(PATHS.manifest.name))
		.pipe(gulp.dest(PATHS.dist));
});

gulp.task('styles', function () {
	console.log('STYLES');
	gulp.src(PATHS.src.styles)
		.pipe($.plumber())
		.pipe($.sass())
		.pipe($.postcss([autoprefixer({browsers: ['last 2 versions']})]))
		.pipe($.rename({extname: '.css'}))
		.pipe(gulp.dest(PATHS.dist))
		.pipe(browserSync.stream());
});

gulp.task('revreplacecss', function () {
	var manifest = gulp.src('./font/' + PATHS.manifest.name);

	gulp.src(['./font/' + FONT_NAME.toLocaleLowerCase() + '.css'])
		.pipe($.revReplace({manifest: manifest}))
		.pipe(gulp.dest(PATHS.dist))
		.pipe($.rev())
		.pipe(gulp.dest(PATHS.dist))
		.pipe($.rev.manifest(PATHS.manifest.name, {merge: true}))
		.pipe(gulp.dest(PATHS.dist));
});

gulp.task('revreplacehtml', function () {
	var manifest = gulp.src('./font/' + PATHS.manifest.name);

	gulp.src(['./font/index.html'])
		.pipe($.revReplace({manifest: manifest}))
		.pipe(gulp.dest(PATHS.dist));
});

gulp.task('browser-sync', function () {
	browserSync.init({
		server: {
			baseDir: PATHS.dist
		},
		port: 9000
	});
});

gulp.task('watch', ['browser-sync'], function () {
	$.watch(PATHS.src.styles, function () {
		gulp.start('styles');
	});

	$.watch(PATHS.src.svg, function () {
		gulp.start('iconfont');
	});

	$.watch(PATHS.src.templates.all, function () {
		gulp.start('iconfont');
	});

	$.watch('./font/*.html', function () {
		browserSync.reload();
	});
});

gulp.task('compile', function () {
	runSequence('clean', 'iconfont', 'revreplacecss', 'revreplacehtml', 'styles');
});

gulp.task('default', ['compile', 'watch']);