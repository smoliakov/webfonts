# Iconto's icon-font generator


![](http://192.168.11.96:8081/app/rest/builds/buildType:(id:webfont_compile)/statusIcon)

## Usage
#### 1. Prepare icons
- icons need to be in .svg format
- icons height must be 750px
- icons artboards must not have padding
- put new icons to the ./svg folder
#### 2. Build font
#
#### - development build with watchers 
```sh
$ sudo npm i
$ ./node_modules/.bin/gulp
```
#### - production build
```sh
$ sudo npm i
$ ./node_modules/.bin/gulp compile
```
##### 3. Get result
- ./font/icontoawesomeneue.css
- ./font/fonts